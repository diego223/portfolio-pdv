function cargarJS() {
  scrollNav();
  aparecer();
}

function scrollNav() {
  document.querySelectorAll(".nav-item").forEach((e) => {
    e.addEventListener("click", function (e) {
      e.preventDefault();
      const t = e.target.attributes.href.value;
      document.querySelector(t).scrollIntoView({ behavior: "smooth" });
    });
  });
}

function aparecer() {
  setTimeout(function () {
    document.body.classList.add('aparecer');
  }, 500);
}

document.addEventListener("DOMContentLoaded", function () {
  cargarJS();
});
